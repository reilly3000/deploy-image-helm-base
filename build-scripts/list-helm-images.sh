#!/bin/bash

set -exo pipefail

cd gitlab

helm init --client-only > /dev/null
helm repo add gitlab https://charts.gitlab.io/ > /dev/null
helm repo update > /dev/null
helm dep update > /dev/null
helm template . --set certmanager-issuer.email=none@none.com | \
   yq -r ". | select( .kind == \"Job\" or .kind == \"Deployment\" or .kind == \"StatefulSet\" or .kind == \"DaemonSet\" ) | .spec.template.spec | [.containers,.initContainers] | .[] | select(.!=null) | .[].image" | \
   sort | uniq

cd ..