# RBAC Exporter

## Overview
The Google GKE Marketplace does not allow Role Based Access Control from
dynamically created Kubernetes Service Accounts. Users must pre-define these
accounts in the marketplace specific `schema.yaml` file.

Rather than updating the `schema.yaml` each time changes are made to service
accounts, the RBAC Exporter automates this process. It provides a library
defining several key features from kubernetes:

- **Chart Objects** are any valid object defined in a ***YAML*** template
  that is exported using `helm template` and may be applied with `kubectl`.
- **Kubernetes Role** defines the metadata describing the role along with
  all of the rules if it is custom. Roles are predefined if not present in
  the provided ***YAML*** input file.
- **Kubernetes Subject** defines any valid Kubernetes Subject, however, the
  library only supports Service Accounts at the current time as this is the
  only subject type supported by Google GKE Marketplace.
- **RBAC Defs** is a module defining useful utility functions for filtering
  and dealing with various metadata under the ***KIND*** property.

## Usage

### Quickstart

Run `convert_chart_roles_to_schema.rb` and read the help information and
pass in the appropriate values.

### Detailed Usage

The `convert_chart_roles_to_schema.rb` script only requires the ***-t*** or
***--template-file*** argument. This represents a ***YAML*** file created by
running `helm template` against the marketplace chart whether it is a normal
chart or a wrapper chart.

If a chart uses dynamic names for service accounts, then the schema.yaml
must use the chart property to define the schema entry versus the actual
service account name. This may be accomplished by defining and maintaining a
mapping file in ****YAML*** format.  For example:

```yaml
---
real_service_account_name: "property.defining.the.name"
```

Pass this file to the script using the optional ***--mapping-file***  argument.

This script output is meant to be injected directly into the relevant
schema.yaml file. The top level of the script's output is not indented. If
it should be indented for the destination, the optional flag ***--indent***
may be set.

Thus, for users who wish to generate the schema entry for service accounts
that must have their names remapped to a property and have a two space
indent:

```sh
convert_chart_roles_to_schema.rb --template-file my_helm_template.yaml \
--mapping-file my_mappings.yaml \
--indent 2
```

## Contributing
Contributors should ensure that any changes pass the existing rspec tests.

From the **rbac-exporter** directory:

```sh
bundle install --binstubs
./bin/rspec --format=doc
```

No contributions that fail to pass all tests will be accepted.
