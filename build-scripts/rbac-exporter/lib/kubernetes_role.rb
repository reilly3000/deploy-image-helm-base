# KubernetesRole
#
# Class describing a Kubernetes Role and the
# rules that define it

class KubernetesRole
  # instance variables
  attr_accessor :role_kind
  attr_accessor :role_name
  attr_accessor :role_namespace
  attr_accessor :predefined
  attr_accessor :rules

  def initialize(role = nil)
    raise ArgumentError, 'Role parameter was nil' if role.nil?

    if %w(Role ClusterRole).include? role["kind"]
      @role_kind = role["kind"]
      @role_name = role["metadata"]["name"]
      @role_namespace = role["metadata"]["namespace"]
    elsif %w(RoleBinding ClusterRoleBinding).include? role["kind"]
      @role_kind = role["roleRef"]["kind"]
      @role_name = role["roleRef"]["name"]
      @role_namespace = role["metadata"]["namespace"]
    else
      raise ArgumentError, 'Role parameter may only be a role or rolebinding.'
    end
    # always assume it's defined elsewhere, custom rules must be added
    @predefined = true
  end

  def config
    return { "type" => role_kind, "rulesType" => "PREDEFINED", "rulesFromRoleName" => role_name } if predefined

    gke_supported_rules = %w(apiGroups resources verbs)
    gke_rules = rules.map { |r| r.select { |k, _| gke_supported_rules.include? k } }

    { "type" => role_kind, "rulesType" => "CUSTOM", "rules" => gke_rules }
  end

  def define_rules(role_entry = nil)
    return if role_entry.nil?

    self.predefined = false
    self.rules = role_entry["rules"]
  end

  def ==(other)
    role_kind == other.role_kind &&
      role_name == other.role_name &&
      role_namespace == other.role_namespace
  end

  def to_s
    <<~EOS
      Role Kind:      #{@role_kind}
      Role Name:      #{@role_name}
      Role Namespace: #{@role_namespace}
    EOS
  end
end
